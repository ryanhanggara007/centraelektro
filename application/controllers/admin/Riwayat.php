<?php
class Riwayat extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url();
            redirect($url);
        };
		$this->load->model('m_kategori');
		$this->load->model('m_barang');
		$this->load->model('m_suplier');
		$this->load->model('m_pembelian');
		$this->load->model('m_penjualan');
		$this->load->model('m_laporan');
	}
	function index(){
	if($this->session->userdata('akses')=='1'){
		//$data['data']=$this->m_barang->tampil_barang();
		$data['query'] = $this->db->get('tbl_jual')->result();
		// $data['kat']=$this->m_kategori->tampil_kategori();
		// $data['jual_bln']=$this->m_laporan->get_bulan_jual();
		// $data['jual_thn']=$this->m_laporan->get_tahun_jual();
		$this->load->view('admin/v_riwayat_faktur',$data);
	}else{
        echo "Halaman tidak ditemukan";
    }
	}

	// function riwayat_faktur()
	// {
	// 	$pillap = $this->input->post('pillap');
	// 	$nofak = $this->input->post('nofak');
		
	// 	if ($pillap == "lapfak") {
	// 		// laporan barang
	// 		$this->lap_data_faktur($nofak);
	// 	}else{
	// 		// laporan penjualans
	// 	}
	// }
	
//tadi ditambahkan di atas , padahal kita fetching data pake request post. Jadi seharusnya pake input->post()
	function lap_data_faktur(){
		$jual_nofak = $this->input->post('jual_nofak');
		$this->db->select("jual_nofak,jual_tanggal,d_jual_barang_id,d_jual_barang_nama,jual_pembeli,jual_user_id,d_jual_barang_satuan,d_jual_barang_harpok,d_jual_barang_harjul,d_jual_qty,d_jual_diskon,d_jual_total");
		$this->db->from('tbl_jual');
		$this->db->join('tbl_detail_jual', 'tbl_detail_jual.d_jual_nofak=tbl_jual.jual_nofak');
	
		$this->db->where('jual_nofak', $jual_nofak);
		
		$this->db->order_by('jual_nofak');
		$query = $this->db->get();
		$x['data'] = $query;

		$this->db->select("jual_nofak,jual_tanggal,d_jual_barang_id,d_jual_barang_nama,d_jual_barang_satuan,jual_pembeli,jual_user_id,d_jual_barang_harpok,d_jual_barang_harjul,d_jual_qty,d_jual_diskon,SUM(d_jual_total) as total, jual_total, jual_jml_uang, jual_kembalian");
		$this->db->from('tbl_jual');
		$this->db->join('tbl_detail_jual', 'tbl_detail_jual.d_jual_nofak=tbl_jual.jual_nofak');
		
		$this->db->where('jual_nofak', $jual_nofak);
		
		
		$this->db->order_by('jual_nofak');
		$x['jml'] = $this->db->get();
		$this->load->view('admin/laporan/v_faktur_riwayat',$x);
	}
	

}