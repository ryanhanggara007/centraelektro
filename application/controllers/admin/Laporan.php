<?php
class Laporan extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url();
            redirect($url);
        };
		$this->load->model('m_kategori');
		$this->load->model('m_barang');
		$this->load->model('m_suplier');
		$this->load->model('m_pembelian');
		$this->load->model('m_penjualan');
		$this->load->model('m_laporan');
	}
	function index(){
	if($this->session->userdata('akses')=='1'){
		$data['data']=$this->m_barang->tampil_barang();
		$data['kat']=$this->m_kategori->tampil_kategori();
		$data['jual_bln']=$this->m_laporan->get_bulan_jual();
		$data['jual_thn']=$this->m_laporan->get_tahun_jual();
		$this->load->view('admin/v_laporan',$data);
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	function select_laporan()
	{
		$pillap = $this->input->post('pillap');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$awal = $this->input->post('awal');
		$akhir = $this->input->post('akhir');

		if ($pillap == "lapbar") {
			// laporan barang
			$this->lap_data_barang($bulan, $tahun, $awal, $akhir);
		}else{
			// laporan penjualan
		}
		if ($pillap == "lappen") {
			// laporan barang
			$this->lap_data_penjualan($bulan, $tahun, $awal, $akhir);
		}else{
			// laporan penjualan
		}
		if ($pillap == "lapfak") {
			// laporan barang
			$this->lap_data_faktur($bulan, $tahun, $awal, $akhir);
		}else{
			// laporan penjualan
		}
	}
	// function lap_stok_barang(){
	// 	$x['data']=$this->m_laporan->get_stok_barang();
	// 	$this->load->view('admin/laporan/v_lap_stok_barang',$x);
	// }
	function lap_data_barang($bulan = null, $tahun = null, $awal = null, $akhir = null){
		// $x['data']=$this->m_laporan->get_data_barang();
		// $this->db->query("SELECT kategori_id,barang_id,kategori_nama,barang_nama,barang_satuan,barang_harjul,barang_stok FROM tbl_kategori JOIN tbl_barang ON kategori_id=barang_kategori_id GROUP BY kategori_id,barang_nama");
		$this->db->select("kategori_id, barang_id, kategori_nama, barang_nama, barang_satuan, barang_harjul, barang_stok, barang_tgl_input");
		$this->db->from('tbl_kategori');
		$this->db->join('tbl_barang', 'tbl_barang.barang_kategori_id = tbl_kategori.kategori_id');
		if ($bulan != null) {
			$this->db->where(['month(barang_tgl_input)' => $bulan]);
		}
		if ($tahun != null) {
			$this->db->where(['year(barang_tgl_input)' => $tahun]);
		}
		if ($awal != null && $akhir != null) {
			$this->db->where(['day(barang_tgl_input)>=' => $awal]);
			$this->db->where(['day(barang_tgl_input)<=' => $akhir]);
		}
		$this->db->group_by('barang_harjul, barang_stok');
		$x['data'] = $this->db->get();
		$this->load->view('admin/laporan/v_lap_barang',$x);
	}

	function lap_data_penjualan($bulan = null, $tahun = null, $awal = null, $akhir = null){
		// $x['data']=$this->m_laporan->get_data_penjualan();

		$this->db->select("jual_nofak,jual_tanggal,jual_total,d_jual_barang_id,d_jual_barang_nama,jual_pembeli,jual_user_id,d_jual_barang_satuan,d_jual_barang_harpok,d_jual_barang_harjul,d_jual_qty,d_jual_diskon,d_jual_total");
		$this->db->from('tbl_jual');
		$this->db->join('tbl_detail_jual', 'tbl_detail_jual.d_jual_nofak=tbl_jual.jual_nofak');
		if ($bulan != null) {
			$this->db->where(['month(jual_tanggal)' => $bulan]);
		}
		if ($tahun != null) {
			$this->db->where(['year(jual_tanggal)' => $tahun]);
		}
		if ($awal != null && $akhir != null) {
			$this->db->where(['day(jual_tanggal)>=' => $awal]);
			$this->db->where(['day(jual_tanggal)<=' => $akhir]);
		}
		$this->db->order_by('jual_nofak', 'DESC');
		$x['data'] = $this->db->get();

		$this->db->select("jual_nofak,jual_tanggal,jual_total,d_jual_barang_id,d_jual_barang_nama,jual_pembeli,jual_user_id,d_jual_barang_satuan,d_jual_barang_harpok,d_jual_barang_harjul,d_jual_qty,d_jual_diskon,sum(d_jual_total) as total");
		$this->db->from('tbl_jual');
		$this->db->join('tbl_detail_jual', 'tbl_detail_jual.d_jual_nofak=tbl_jual.jual_nofak');
		if ($bulan != null) {
			$this->db->where(['month(jual_tanggal)' => $bulan]);
		}
		if ($tahun != null) {
			$this->db->where(['year(jual_tanggal)' => $tahun]);
		}
		if ($awal != null && $akhir != null) {
			$this->db->where(['day(jual_tanggal)>=' => $awal]);
			$this->db->where(['day(jual_tanggal)<=' => $akhir]);
		}
		$this->db->order_by('jual_nofak', 'DESC');
		$x['jml'] = $this->db->get();
		// $x['jml']=$this->m_laporan->get_total_penjualan();
		
		
		$this->load->view('admin/laporan/v_lap_penjualan',$x);
	}

	function lap_data_faktur($bulan = null, $tahun = null, $awal = null, $akhir = null){
		$this->db->select("jual_nofak,jual_tanggal,d_jual_barang_id,d_jual_barang_nama,jual_pembeli,jual_user_id,d_jual_barang_satuan,d_jual_barang_harpok,d_jual_barang_harjul,d_jual_qty,d_jual_diskon,d_jual_total");
		$this->db->from('tbl_jual');
		$this->db->join('tbl_detail_jual', 'tbl_detail_jual.d_jual_nofak=tbl_jual.jual_nofak');
		if ($bulan != null) {
			$this->db->where(['month(jual_tanggal)' => $bulan]);
		}
		if ($tahun != null) {
			$this->db->where(['year(jual_tanggal)' => $tahun]);
		}
		if ($awal != null) {
			$this->db->where(['day(jual_tanggal)' => $awal]);
		}
		$this->db->order_by('jual_nofak');
		$x['data'] = $this->db->get();

		$this->db->select("jual_nofak,jual_tanggal,d_jual_barang_id,d_jual_barang_nama,d_jual_barang_satuan,jual_pembeli,jual_user_id,d_jual_barang_harpok,d_jual_barang_harjul,d_jual_qty,d_jual_diskon,SUM(d_jual_total) as total, jual_total, jual_jml_uang, jual_kembalian");
		$this->db->from('tbl_jual');
		$this->db->join('tbl_detail_jual', 'tbl_detail_jual.d_jual_nofak=tbl_jual.jual_nofak');
		if ($bulan != null) {
			$this->db->where(['month(jual_tanggal)' => $bulan]);
		}
		if ($tahun != null) {
			$this->db->where(['year(jual_tanggal)' => $tahun]);
		}
		if ($awal != null) {
			$this->db->where(['day(jual_tanggal)' => $awal]);
		}
		$this->db->order_by('jual_nofak', 'DESC');
		$x['jml'] = $this->db->get();
		$this->load->view('admin/laporan/v_faktur',$x);
	}
	
	// function lap_penjualan_pertanggal(){
	// 	$tanggal=$this->input->post('tgl');
	// 	$x['jml']=$this->m_laporan->get_data__total_jual_pertanggal($tanggal);
	// 	$x['data']=$this->m_laporan->get_data_jual_pertanggal($tanggal);
	// 	$this->load->view('admin/laporan/v_lap_jual_pertanggal',$x);
	// }
	// function lap_penjualan_perbulan(){
	// 	$bulan=$this->input->post('bln');
	// 	$x['jml']=$this->m_laporan->get_total_jual_perbulan($bulan);
	// 	$x['data']=$this->m_laporan->get_jual_perbulan($bulan);
	// 	$this->load->view('admin/laporan/v_lap_jual_perbulan',$x);
	// }
	// function lap_penjualan_pertahun(){
	// 	$tahun=$this->input->post('thn');
	// 	$x['jml']=$this->m_laporan->get_total_jual_pertahun($tahun);
	// 	$x['data']=$this->m_laporan->get_jual_pertahun($tahun);
	// 	$this->load->view('admin/laporan/v_lap_jual_pertahun',$x);
	// }
	// function lap_laba_rugi(){
	// 	$bulan=$this->input->post('bln');
	// 	$x['jml']=$this->m_laporan->get_total_lap_laba_rugi($bulan);
	// 	$x['data']=$this->m_laporan->get_lap_laba_rugi($bulan);
	// 	$this->load->view('admin/laporan/v_lap_laba_rugi',$x);
	// }
}