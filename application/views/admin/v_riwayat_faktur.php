 <!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Produk By Bahyu Sanciko">
    <meta name="author" content="Bahyu Sanciko">

    <title>Riwayat Faktur Penjualan</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url().'assets/css/bootstrap.min.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/style.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/font-awesome.css'?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url().'assets/css/4-col-portfolio.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/dataTables.bootstrap.min.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/jquery.dataTables.min.css'?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap-datetimepicker.min.css'?>">
    <link href="<?php echo base_url().'assets/dist/css/bootstrap-select.css'?>" rel="stylesheet">

</head>

<body>

    <!-- Navigation -->
   <?php 
        $this->load->view('admin/menu');
   ?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Riwayat
                    <small>Faktur Penjualan</small>
                </h1>
            </div>
        </div>
        <!-- /.row -->
        <!-- Projects Row -->


 <form action="<?= base_url('admin/riwayat/lap_data_faktur')?>" method="post">
        <div class="box-body">
            <div class="form-group row">
                <div class="col-md-2"><label for="jual_nofak">No Faktur :</label></div>
                <div class="col-md-10">
                	<!----
tinggal yang select dinganti input aja ngab

                	-->
                	 <th style="text-align:right;"><select name="jual_nofak" id="jual_nofak" class="selectpicker show-tick form-control" data-live-search="true" title="Pilih No Faktur"  style="text-align:right;margin-bottom:5px;" required>
                        <!-- <option value="">Pilih Barang</option> -->
                        <?php foreach ($query as $row ): ?>
                        <option value="<?php echo $row->jual_nofak; ?>">
                            <?php echo $row->jual_nofak; ?>
                         
                        </option>
                          <?php endforeach; ?>
                    </select> 
                	 </th>
                   
                </div>
            </div>
     
        <div class="box-footer">
            <button type="submit" class="btn btn-success pull-right">Cetak</button>
        </div>
    </form>
</div>
           
                  


           
        
        
 

      

        <!-- Footer -->
        <footer>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="<?php echo base_url().'assets/js/jquery.js'?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url().'assets/dist/js/bootstrap-select.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dataTables.bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/jquery.dataTables.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/moment.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap-datetimepicker.min.js'?>"></script>
    <script type="text/javascript">
            $(function () {
                $('#datetimepicker').datetimepicker({
                    format: 'DD MMMM YYYY HH:mm',
                });
                
                $('#datepicker').datetimepicker({
                    format: 'YYYY-MM-DD',
                });
                $('#datepicker2').datetimepicker({
                    format: 'YYYY-MM-DD',
                });

                $('#timepicker').datetimepicker({
                    format: 'HH:mm'
                });
            });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#mydata').DataTable();
        } );
    </script>
    
</body>

</html>
