<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Produk By Bahyu Sanciko">
    <meta name="author" content="Bahyu Sanciko">

    <title>Laporan</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url().'assets/css/bootstrap.min.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/style.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/font-awesome.css'?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url().'assets/css/4-col-portfolio.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/dataTables.bootstrap.min.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/jquery.dataTables.min.css'?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap-datetimepicker.min.css'?>">
    <link href="<?php echo base_url().'assets/dist/css/bootstrap-select.css'?>" rel="stylesheet">

</head>

<body>

    <!-- Navigation -->
   <?php 
        $this->load->view('admin/menu');
   ?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Data
                    <small>Laporan</small>
                </h1>
            </div>
        </div>
        <!-- /.row -->
        <!-- Projects Row -->
        
 

        <form action="<?= base_url('admin/laporan/select_laporan')?>" method="post">
        <div class="box-body">
            <div class="form-group row">
                <div class="col-md-2"><label for="pillap">Laporan :</label></div>
                <div class="col-md-10">
                    <select name="pillap" id="pillap" class="form-control" required="">
                        <option value="">-- Pilih Laporan --</option>
                        <option value="lapbar">Laporan Barang</option>
                        <option value="lappen">Laporan Penjualan</option>
                      <!--   <option value="lapfak">Laporan faktur</option> -->
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2"><label for="bulan/tahun">Bulan / Tahun :</label></div>
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-4">
                            <select name="bulan" id="bulan" class="form-control" required="">
                                <option value="">-- Pilih Bulan --</option>
                                <option value="01">Januari</option>
                                <option value="02">Februari</option>
                                <option value="03">Maret</option>
                                <option value="04">April</option>
                                <option value="05">Mei</option>
                                <option value="06">Juni</option>
                                <option value="07">Juli</option>
                                <option value="08">Agustus</option>
                                <option value="09">September</option>
                                <option value="10">Oktober</option>
                                <option value="11">November</option>
                                <option value="12">Desember</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <select name="tahun" id="tahun" class="form-control" required="">
                                <option value="">-- Pilih Tahun --</option>
                                <?php for ($i= (date('Y') - 0); $i < (date('Y') + 5); $i++): ?>
                                    <option value="<?= $i ?>"><?= $i ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2"><label for="awal">Tanggal :</label></div>
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-2">
                            <select name="awal" id="awal" class="form-control" required="">
                                <option value="">Awal</option>
                                <?php for ($i= 1; $i <= 31; $i++): ?>
                                    <option value="<?= ($i < 10) ? '0' . $i : $i ?>"><?= $i ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <div class="col-md-1 text-center" style="padding-top: 10px">
                            <label for="akhir">Sampai</label>
                        </div>
                        <div class="col-md-2">
                            <select name="akhir" id="akhir" class="form-control">
                                <option value="">Akhir</option>
                                <?php for ($i= 1; $i <= 31; $i++): ?>
                                    <option value="<?= ($i < 10) ? '0' . $i : $i ?>"><?= $i ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-success pull-right">Cetak</button>
        </div>
    </form>
</div>

        <!-- Footer -->
        <footer>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="<?php echo base_url().'assets/js/jquery.js'?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url().'assets/dist/js/bootstrap-select.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dataTables.bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/jquery.dataTables.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/moment.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap-datetimepicker.min.js'?>"></script>
    <script type="text/javascript">
            $(function () {
                $('#datetimepicker').datetimepicker({
                    format: 'DD MMMM YYYY HH:mm',
                });
                
                $('#datepicker').datetimepicker({
                    format: 'YYYY-MM-DD',
                });
                $('#datepicker2').datetimepicker({
                    format: 'YYYY-MM-DD',
                });

                $('#timepicker').datetimepicker({
                    format: 'HH:mm'
                });
            });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#mydata').DataTable();
        } );
    </script>
    
</body>

</html>
